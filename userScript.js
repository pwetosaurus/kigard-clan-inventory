// ==UserScript==
// @name     kigardClanInventory
// @version  1.3.7
// @grant    none
// @include  https://www.kigard.fr/index.php*
// @exclude  https://www.kigard.fr/index.php?p=tourdejeu
// ==/UserScript==

// helpers
const getBuildings = url => {
  // Récupération de la réponse de l'appel GET de la liste des bâtiments du clan.
  const fetchBuildings = fetch(url);
  fetchBuildings.then(response => {
    // On récupère le HTML de la page.
    return response.text();
  }).then(buildingsPageDOM => {
    // On crée un <DIV> et on y injecte le contenu de la page des bâtiments du clan.
    const DOM = document.createElement('div');
    DOM.innerHTML = buildingsPageDOM;
    
    // On filtre sur les lignes de la <TABLE> contenant uniquement des bâtiments, et on crée une liste qui contiendra les meta données des bâtiments.
    const buildingsDOM = DOM.querySelectorAll('#bloc > table > tbody > tr + tr');
    let buildings = new Map();
    
    // On boucle sur la liste des bâtiments de la page
    buildingsDOM.forEach(buildingDOM => {
      // on récupère le lien qu'il y a dans la dernière colonne.
      const linkDOM = buildingDOM.querySelector('td:nth-child(8) a');
      // on récupère les coordonnées du bâtiment
      const position = {
        x: parseInt(buildingDOM.querySelector('td:nth-child(5) center').innerText),
        y: parseInt(buildingDOM.querySelector('td:nth-child(6) center').innerText)
      };
      
      // si on a bien un lien et qu'il contient le mot stock, on a un bâtiment à stock, donc on traîte ses données.
      if(linkDOM && linkDOM.href.match(/stock/g)) {
        // on récupère l'id du bâtiment pour dissocier les bâtiments en doublon
        const key = linkDOM.href.match(/[0-9]*$/);
        // on construit la liste des méta données du bâtiment
        const value = {
          icon: buildingDOM.querySelector('td:first-child img'),
          url: linkDOM.href,
          stock: new Map(),
          name: buildingDOM.querySelector('td:nth-child(1)').innerText.trim().replace('�tabli', 'établi').replace('biblioth�', 'bibliothè'),
          position,
          encumbrance: buildingDOM.querySelector('td:nth-child(8) center').innerText
        }
        
        // on met dans la liste le bâtiment et ses données.
        buildings.set(key, value);
      }
    });
    
    // on renvoie la liste des méta données des bâtiments
    return buildings;
  }).then(buildings => {
    // on crée une liste des objets, qui contient les objets non triés alphabétiquement
    let unsortedInventory = new Map();
        
    // on compte les itérations sur les bâtiments, pour savoir quand on a fini.
    let iteration = 0;
    
    // pour chaque bâtiment…
    buildings.forEach((value, key, list) => {
      // on demande le HTML de la liste des objets du bâtiment.
      const fetchItems = fetch(value.url);
      
      // une fois qu'on a la réponse de la page des objets du bâtiment…
      fetchItems.then(response => {
        // …on veut le HTML
        return response.text();
      }).then(stockItemsDOM => {
        // on compte la nouvelle itération
	      iteration++;
				
        // on crée un <DIV> pour y mettre le HTML de la liste des objets du bâtiment.
        const DOM = document.createElement('div');
        DOM.innerHTML = stockItemsDOM;
        
        // on filtre le HTML pour ne garder que la <TABLE> qui contient les objets
        const table = DOM.querySelector('#table_item tbody');
        // on filtre à nouveau pour ne garder que le nom de l'objet
        const itemsDom = table.querySelectorAll('tr td:first-child > strong');
        
        // Pour chaque titre HTML d'objet du bâtiment
        itemsDom.forEach(itemDOM => {
          // On extrait le nom de l'objet en texte en retirant le balisage HTML, et on corrige les caractères accentués qui ont sauté (je n'ai pas réussi à résoudre les problèmes d'encodage).
          let name = cleanStrings(itemDOM.innerText.trim());
          
          // on copie le nom de l'objet, c'est le nom complet, avec l'enchantement au début.
          let fullName = name.toString();
          
          // on sépare le préfixe du nom de l'objet et on corrige les caractères accentués.
          const prefixDOM = itemDOM.querySelector(".enchantement");
          const prefix = prefixDOM ? cleanStrings(prefixDOM.innerText) : undefined;
          
          // on trouve le nom de l'objet non préfixé (nécessaire pour trier alphabétiquement tout en gardant les objets enchantés près de leur équivalent non enchanté).
          if(prefix) {
	          name = name.replace(prefix, '').trim();
          }
          // on retire les accents du nom pour ne pas fausser le tri alphabétique
          name = unaccentify(name);
          
          // on ajoute 1 au compte de cet objet pour ce bâtiment
          const count = unsortedInventory.get(name) && unsortedInventory.get(name).get(fullName) && unsortedInventory.get(name).get(fullName).count ? unsortedInventory.get(name).get(fullName).count + 1 : 1;
          // on récupère (ou crée) les méta données de l'objet.
          const itemData = unsortedInventory.get(name) || new Map();
          // on met à jour les méta données de l'objet.
          itemData.set(fullName, {count, icon: itemDOM.previousElementSibling, prefix, fullName, watched: watchedItems.indexOf(fullName) !== -1});
          // on ajoute l'objet et ses méta données dans la liste des objets globale.
          unsortedInventory.set(name, itemData);
          // on ajoute l'objet également dans la liste des objets de ce bâtiment uniquement.
          let buildingStock = buildings.get(key).stock;
          // on ajoute 1 au compte de cet objet dans ce bâtiment
          buildingStock.set(fullName, buildingStock.get(fullName) + 1 || 1);
        });
        
        // on regarde s'il nous reste des données à traiter
        const proceed = iteration === list.size;
        
        return {unsortedInventory, buildings, proceed};
      }).then(data => {
        // on récupère les données des bâtiments et l'inventaire global.
        let {unsortedInventory, buildings, proceed} = data;
        
        // s'il ne reste plus de données à traiter, on peut commencer à afficher les données.
        if(proceed) {
          // on crée l'entête de la liste
          const headerWrapper = document.createElement('thead');
          // on ajoute une ligne à l'entête de la liste
          const headerRow = document.createElement('tr');
          headerRow.appendChild(document.createElement('th'));
          
          // pour chacun des bâtiments dont nous avons récupéré les données
          buildings.forEach(building => {
            // on crée une ligne.
            const buildingHead = document.createElement('th');
            // on crée un lien vers l'inventaire du bâtiment
            const buildingLink = document.createElement('a');
            // on crée un <P> qui contiendra l'encombrement du bâtiment.
            const buildingEncumbrance = document.createElement('p');
            // on ajoute la cible du lien du bâtiment, on lui demande de s'ouvrir dans un nouvel onglet et on affiche des données de localisation et de nom au survol.
            buildingLink.href = building.url;
            buildingLink.target = '_blank';
            buildingLink.title = `${building.name} (X: ${building.position.x}/Y: ${building.position.y})`;
            // on insère les données d'encombrement du bâtiment dans le paragraphe dédié et on le met en forme.
            buildingEncumbrance.innerText = building.encumbrance;
            buildingEncumbrance.style.fontSize = "0.7em";
            buildingEncumbrance.style.paddingTop = "5px";
            // on met en forme l'entête.
            buildingHead.style.textAlign = "center";
            // on injecte les données dans les éléments HTML créés.
            buildingLink.appendChild(building.icon);
            buildingHead.appendChild(buildingLink);
            buildingHead.appendChild(buildingEncumbrance);
            headerRow.appendChild(buildingHead);
            // on injecte la ligne dans l'entête de la liste.
            headerWrapper.appendChild(headerRow);
          });
          
          // on injecte l'entête de la liste dans la liste des objets du plugin.
          headerRow.appendChild(document.createElement('th'));
          inventoryContainer.appendChild(headerWrapper);
          
          // on crééun élément HTML pour contenir la liste des objets.
          const inventoryWrapper = document.createElement('tbody');
          
          // on crée une variable qui contiendra les objets de l'inventaire triés alphabétiquement.
          const inventory = new Map();
          // on crée une liste des noms, pour pouvoir les ordonner alphabétiquement.
          const itemsName = [];
          // on ajoute les objets surveillés dans la liste des noms en en retirant les accents
          watchedItems.forEach(watchItem => {
            itemsName.push(unaccentify(watchItem));
          });
          
          // pour chaque objet que possède le clan
          unsortedInventory.forEach((value, key) => {
            // on met le nom dans la liste des noms.
            itemsName.push(unaccentify(key));
          });
          // on trie les noms alphabétiquement.
          itemsName.sort();
          
          // pour chacun des noms d'objets
          itemsName.forEach(name => {
            // on crée des valeurs vides pour les objets que l'on surveille, mais qui n'ont pas de données
            const data = unsortedInventory.get(name) || new Map();
            
            // si l'objet que l'on surveille n'est pas présent en stock, on crée le minimum d'informations pour l'affichage.
            if(!unsortedInventory.get(name)) {
              const falseData = {icon: document.createElement("img"), count: 0, watched: true};
              data.set(name, falseData);
            }
            // on met dans l'inventaire tous les objets que l'on possède d'un type donné (on va ainsi avoir toutes les données dans le même ordre que les noms d'objets, et donc triés alphabétiquement (sans le préfixe d'enchantement).
          	inventory.set(name, data);
          });
          
          // pour chaque objet de l'inventaire
          inventory.forEach((inventorySubItems, inventoryKey) => {
            // pour chaque version de l'objet (donc les différents enchantements).
            inventorySubItems.forEach(inventoryValue => {
              // on crée une ligne
              const itemRow = document.createElement('tr');
              // on ajoute de la couleur si c'est un objet que l'on surveille.
              if(inventoryValue.watched) {
                itemRow.style.color = "white";
                itemRow.style.backgroundColor = inventoryValue.count ? "green" : "red";
              }
              // on crée une première colonne pour son nom
              const itemHead = document.createElement('td');
              // on crée une autre colonne pour le total d'objets que l'on possède dans les différents bâtiments.
              const itemTail = document.createElement('td');
            	
              // on met l'icône de l'objet dans la première colonne…
              itemHead.appendChild(inventoryValue.icon);
            	
              // …puis on met le nom de l'objet
              itemHead.append(` ${inventoryValue.fullName || inventoryKey}`);

              // et on met le nombre d'objets de ce type possédé dans la dernière colonne.
              itemTail.innerText = `(${inventoryValue.count})`;
            
              // on pousse la première colonne dans la ligne
              itemRow.appendChild(itemHead);

              // pour chaque bâtiment…
              buildings.forEach((buildingValue, buildingKey) => {
                // on crée une colonne
                const buildingCol = document.createElement('td');
                // on crée un lien vers le bâtiment
                const buildingLink = document.createElement('a');
                // on met à jour le lien, on lui dit de s'ouvrir dans un nouvel onglet, on met le compte d'objets dans le texte et on affiche des informations au survol
                buildingLink.href = buildingValue.url;
                buildingLink.target = '_blank';
                buildingLink.innerText = buildingValue.stock.get(inventoryValue.fullName) || '-';
                buildingLink.title = `${buildingValue.name} (X: ${buildingValue.position.x}/Y: ${buildingValue.position.y})`;
                // on met le lien dans la colonne et on le met en forme
                buildingCol.appendChild(buildingLink);
                buildingCol.style.textAlign = "center";

                // on pousse la colonne dans la ligne de l'objet
                itemRow.appendChild(buildingCol);
              });

              // maintenant que toutes les colonnes ont été ajoutées, on injecte la dernière dans la ligne (avec le total des objets de ce type dans les différents bâtiments).
              itemRow.appendChild(itemTail);

              // on ajoute la ligne dans l'inventaire.
              inventoryWrapper.appendChild(itemRow);
            });
          
            // on ajoute l'inventaire dans la page.
          	inventoryContainer.appendChild(inventoryWrapper);
          });
        }
      });
    });
  });
};

// on ajoute une entrée de menu dans inventaire
const menu = document.querySelector('#menu li:nth-child(3) ul');
const menuItem = document.createElement('li');
const menuItemLink = document.createElement('a');
menuItemLink.href = '/index.php?p=inventaireClan';
menuItemLink.innerText = 'Clan';

menuItem.appendChild(menuItemLink);

menu.appendChild(menuItem);
        
// fonction qui corrige les caractères mal encodés
const cleanStrings = string => {
  return string
    .replace(/�/g, 'é')
    .replace(' é ', ' à ')
    .replace('rédeur', 'rôdeur')
    .replace('maétre', 'maître')
    .replace(/(C|c)réne/, '$1râne')
    .replace(/(P|p)oussiére/, '$1oussière')
    .replace(/(R|r)agoét/, '$1agoût')
    .replace(/(b|B)éton/,'$1âton')
    .replace(/(j|J)ambiére/,'$1ambière')
    .replace(/(e|é)béne/,'ébène')
    .replace(/(E|É)béne/,'Ébène')
    .replace(/(T|t)réfle/, '$1rèfle')
    .replace('hallucinogéne', 'hallucinogène')
    .replace(/(F|f)léche/, '$1lèche')
}

// fonction qui désaccentue les caractères
const unaccentify = string => {
  return string
    .replace(/[àâáä]/g, "a")
    .replace(/[èêéë]/g, "e")
    .replace(/[ìîíï]/g, "i")
    .replace(/[òôóö]/g, "o")
    .replace(/[ùûúü]/g, "u")
    .replace(/[ÀÂÁÄ]/g, "A")
    .replace(/[ÈÊÉË]/g, "E")
    .replace(/[ÌÎÍÏ]/g, "I")
    .replace(/[ÒÔÓÖ]/g, "O")
    .replace(/[ÙÛÚÜ]/g, "U")
}
        
// on crée un élément HTML <TABLE> qui contiendra l'inventaire mis en forme et trié.
const inventoryContainer = document.createElement('table');

// on ajoute les éléments que l'on veut surveiller.
const watchedItems = [
//	"Brosse à dents",
//  "Peigne",
//  "Coton tige"
];

// on crée une fausse page qui déclenchera le script à l'affichage
const displayClanInventory = document.location.href === 'https://www.kigard.fr/index.php?p=inventaireClan';

// on vide le contenu de la page quand on est sur la fausse page.
if(displayClanInventory) {
  // on modifie le titre de la fausse page
  const container = document.getElementById('bloc');
  container.innerText = '';
  
  const title = document.createElement('h3');
  title.innerText = 'Inventaire du Clan';
  
  // on met le titre dans la page
  container.appendChild(title);
  
  // on met l'inventaire dans la page
  container.appendChild(inventoryContainer);
  
  // on construit le contenu de la liste des objets.
  getBuildings('https://www.kigard.fr/index.php?p=clan&g=batiments');
}

